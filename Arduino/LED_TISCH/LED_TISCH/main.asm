.include "m328pdef.inc" 

.def delay1	= r17
.def delay2	= r18
.def delayv	= r19
.def tmp = r20
.equ DATA	= 5 ; PORTB bit number to blink LED on

	rjmp main

delay:
	clr	delay1
	clr	delay2
	ldi	delayv, 100
      
delay_loop: 
	dec	delay2		
	brne	delay_loop 	
	dec	delay1		
	brne	delay_loop 	
	dec	delayv		
	brne	delay_loop 	
	ret     		; go back to where we came from


main:
	ldi     tmp, HIGH(RAMEND)
	out     SPH, tmp
	ldi     tmp, LOW(RAMEND)     ; Stackpointer initialisieren
	out     SPL, tmp
	
	sbi	DDRB, DATA	; connect PORTD pin 4 to LED
	ldi tmp,47
loop:

   
	dec tmp
	brne weiter
	rcall	delay	
	 ldi tmp,47
	 ldi ZH,01
	 ldi ZL,00
	

weiter:
    LD tmp, Z+
 
	sbi	PORTB, DATA	; turn PD4 HIGH
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	cbi	PORTB, DATA	; turn PD4 low
	
	
	
	
	
		; delay again for a short bit
	rjmp	loop		; recurse back to the head of loop
